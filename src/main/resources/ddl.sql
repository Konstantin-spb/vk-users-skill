-- we don't know how to generate root <with-no-name> (class Root) :(
create table app_user_profiles
(
	id bigserial not null
		constraint app_user_profiles_pkey
			primary key,
	type varchar(15) not null
		constraint uk9ig24v1phmav6d1rlnqgyu604
			unique
		constraint uk_9ig24v1phmav6d1rlnqgyu604
			unique
);

alter table app_user_profiles owner to vkadmin;

create table status
(
	id bigserial not null
		constraint status_pkey
			primary key,
	status varchar(50) not null
		constraint uk_kxaj0dvn13fwjuimg3y2j0oa2
			unique
		constraint ukkxaj0dvn13fwjuimg3y2j0oa2
			unique
);

alter table status owner to vkadmin;

create table app_users
(
	id bigserial not null
		constraint app_users_pkey
			primary key,
	email varchar(100) not null
		constraint uk4vj92ux8a2eehds1mdvmks473
			unique
		constraint uk_4vj92ux8a2eehds1mdvmks473
			unique,
	first_name varchar(30),
	login_name varchar(30)
		constraint uk_ravb2y8ii2psxn1ekbc4eittg
			unique
		constraint ukravb2y8ii2psxn1ekbc4eittg
			unique,
	password varchar(255) not null,
	surname varchar(30),
	status_id bigint not null
		constraint fkom3l1feo1j9alnt0m247xs3lh
			references status
);

alter table app_users owner to vkadmin;

create table vk_users
(
	user_id bigint not null
		constraint users_pkey
			primary key,
	access_token varchar(100) not null,
	app_user_id bigint
		constraint vk_users_app_users_id_fk
			references app_users
				on update cascade on delete cascade,
	alias varchar(50),
	login varchar(50) default 0,
	password varchar(100) default 0
);

alter table vk_users owner to vkadmin;

create unique index users_user_id_uindex
	on vk_users (user_id);

create unique index users_access_token_uindex
	on vk_users (access_token);

create table app_user_app_user_profile
(
	app_user_profile_id bigint not null
		constraint fk6mt5ryybvk7estlbod2xlwdc
			references app_user_profiles,
	app_user_id bigint not null
		constraint fkqyoxsj486qd81f1shk9v566og
			references app_users,
	constraint app_user_app_user_profile_pkey
		primary key (app_user_id, app_user_profile_id)
);

alter table app_user_app_user_profile owner to vkadmin;

create table autorization_history
(
	id bigserial not null
		constraint autorization_history_pkey
			primary key,
	browser_agent varchar(255),
	date_login timestamp not null,
	ip_address varchar(15) not null,
	app_user_id bigint not null
		constraint fk8ek247qr5otd5xp0np14jpc8m
			references app_users
);

alter table autorization_history owner to vkadmin;

create table vk_groups
(
	id bigserial not null
		constraint vk_groups_pk
			primary key,
	group_link varchar(50) not null
);

alter table vk_groups owner to vkadmin;

create unique index vk_groups_id_uindex
	on vk_groups (id);

create unique index vk_groups_group_link_uindex
	on vk_groups (group_link);

create table phrases_types
(
	id serial not null
		constraint phrases_types_pk
			primary key,
	phrase_type varchar(50) not null
);

alter table phrases_types owner to vkadmin;

create table good_words
(
	id bigserial not null
		constraint good_words_pk
			primary key,
	phrase text not null,
	phrase_type_id integer
		constraint good_words_phrases_types_id_fk
			references phrases_types
				on update cascade on delete cascade
);

alter table good_words owner to vkadmin;

create unique index good_words_column_1_uindex
	on good_words (id);

create unique index good_words_phrase_uindex
	on good_words (phrase);

create unique index phrases_types_id_uindex
	on phrases_types (id);

create unique index phrases_types_phrase_type_uindex
	on phrases_types (phrase_type);


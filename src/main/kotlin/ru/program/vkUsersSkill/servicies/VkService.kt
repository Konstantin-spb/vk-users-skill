package ru.program.vkUsersSkill.servicies

import com.google.gson.JsonObject
import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.exceptions.ApiCaptchaException
import com.vk.api.sdk.objects.likes.responses.AddResponse
import com.vk.api.sdk.objects.messages.responses.GetResponse
import com.vk.api.sdk.objects.users.UserXtrCounters
import com.vk.api.sdk.queries.groups.GroupField
import com.vk.api.sdk.queries.likes.LikesType
import com.vk.api.sdk.queries.users.UserField
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import ru.program.vkUsersSkill.models.FriendshipUsers
import ru.program.vkUsersSkill.repositories.FriendshipUsersRepository
import ru.program.vkUsersSkill.repositories.GoodWordRepository
import ru.program.vkUsersSkill.repositories.VkUserRepository
import ru.program.vkUsersSkill.utils.VkUtils
import java.util.*
import kotlin.random.Random

@Component
@Scope("prototype")
class VkService {
    val LOGGER = LogManager.getLogger(VkService::class.java)

    //    @Autowired
    lateinit var vkApiClient: VkApiClient

    @Autowired
    lateinit var vkUserRepository: VkUserRepository

    @Autowired
    lateinit var goodWordRepository: GoodWordRepository

    @Autowired
    lateinit var vkUtils: VkUtils

    @Autowired
    lateinit var friendshipUsersRepository: FriendshipUsersRepository

    var commentsCount: Int = 0
//    private val user = vkUserRepository.getOne(fromUser)

    fun sendMessage(fromUser: Long, toUser: Long, messageText: String = "", messageOrSticker: Boolean = false) {
        val userActor = vkUtils.getUserActor(fromUser)
        val goodWord = goodWordRepository.findAll()
        goodWord.shuffle()
        val user = vkUserRepository.getOne(fromUser)

        val messageSender = vkApiClient.messages().send(userActor).userId(toUser.toInt())
        when (messageOrSticker) {
            false -> if (messageText.isEmpty()) {
                messageSender.message(goodWord.first().phrase)
                LOGGER.info("Send message to ${toUser}: \"${goodWord.first().phrase}\" ")
            } else {
                messageSender.message(messageText)
                LOGGER.info("Send message to ${toUser}: \"STIKER\" ")
            }
            true -> messageSender.stickerId(Random.nextInt(1, 13))
        }
        messageSender.execute()
        //vkApiClient.messages().send(userActor).message(goodWord.first().phrase).userId(toUser.toInt()).execute()
    }

    //46800000 - 14 часов в миллисекундах
    //46800000 / 40 = 1170000
    fun addCommentToPost(groupIdText: String, fromUser: Long): String {
        val vkUser = vkUserRepository.getOne(fromUser)
        val userActor = UserActor(vkUser.userId.toInt(), vkUser.accessToken)
        val fields: List<GroupField> = listOf(
                GroupField.IS_CLOSED,
                GroupField.DEACTIVATED,
                GroupField.TYPE,
                GroupField.IS_MEMBER,
                GroupField.AGE_LIMITS,
                GroupField.CAN_CREATE_TOPIC,
                GroupField.CAN_POST
        )

        val answer = vkApiClient.groups().getById(userActor).groupIds(groupIdText).fields(fields).execute()
        var groupId: Long = 0
//        groupId = if (answer.first().id.toLong() > 0 && answer.first().id.toLong() != 0L) groupId * -1 else groupId
        groupId = groupIdText.toLong() * -1
        val groupWall = vkApiClient.wall().get(userActor).ownerId(groupId.toInt()).execute()
        groupWall.items.sortByDescending { it.date }
        val goodWord = goodWordRepository.findAll()
        goodWord.shuffle()
        println(groupWall.items.first().ownerId)
//        println(groupWall)
        val postId = groupWall.items.first().id
//        vkApiClient.wall().createComment(userActor,postId).ownerId(groupId.toInt()).stickerId(1).execute()
        val createPCommentResponse = vkApiClient.wall().createComment(userActor, postId).ownerId(groupId.toInt()).message(goodWord.first().phrase).execute()
//        groupWall.items.forEach { println(it.date) }
//        println(groupWall)
        println(createPCommentResponse)
        println("Group id - ${groupId}")
        return createPCommentResponse.toString()
    }

    fun addLikeToAva(fromUser: Long, toUser: Long): Boolean {
        val userActor = vkUtils.getUserActor(fromUser)
        val vkResponse = vkApiClient.users().get(userActor).userIds(toUser.toString()).fields(UserField.PHOTO_ID).execute()
        var photoId = -1
        if (vkResponse.first().photoId != null) {
            photoId = vkResponse.first().photoId.substringAfterLast("_").toInt()
        }
        var toLikeDone = false

        if (photoId > 0) {
            Thread.sleep(1000)
            val haveMyLike = vkUtils.isLiked(fromUser, toUser, photoId.toLong())
            when {
                haveMyLike.not() -> {
                    Thread.sleep(340)

                    var like: AddResponse = AddResponse()
                    try {
                        like = vkApiClient.likes().add(userActor, LikesType.PHOTO, photoId).ownerId(toUser.toInt()).execute()
                    } catch (e: ApiCaptchaException) {
                        val captchaSid = e.sid
                        val captchaImg = e.image

                        LOGGER.info(captchaImg)
                        val input = Scanner(System.`in`)
                        val captchaKey = input.next()

                        e.printStackTrace()
                        vkApiClient.likes()
                                .add(userActor, LikesType.PHOTO, photoId)
                                .ownerId(toUser.toInt())
                                .captchaSid(captchaSid)
                                .captchaKey(captchaKey)
                                .execute()
                    }
                    LOGGER.info(like)
                    LOGGER.info("Add like to https://vk.com/id${toUser}")
                    toLikeDone = true
                }
                haveMyLike -> LOGGER.info("https://vk.com/id${toUser} уже лайкали эту аву.")
                else -> LOGGER.info("https://vk.com/id${toUser} не имеет аватарки или профиль закрыт.")
            }
        }
        return toLikeDone
//        vkApiClient.likes().isLiked()
    }

    fun getAllOnlineMembersFromGroup(fromUser: Long, groupNameOrId: String): MutableList<UserXtrCounters> {
        val allMembers = getAllGroupMembers(fromUser, groupNameOrId)
        val membersOnline = getOnlineMembersFromList(fromUser, allMembers)
        println()
        return membersOnline
    }

    fun getOnlineMembersFromList(fromUser: Long, users: List<Int>): MutableList<UserXtrCounters> {
        var start = 0
        var end = 999
        var offset = 1000
//        var step = 1000
//        var lastStep = 0
        val stringUsers = users.map { it.toString() }
        val membersOnline: MutableList<UserXtrCounters> = mutableListOf()
        val circles = stringUsers.size / 1000 + 1//получаем количество циклов
        for (count in 1..circles) {
            val r = stringUsers.subList(start, end)
            if (r.isNotEmpty()) {
                val l = vkUtils.checkUserOnline(fromUser, r)
                if (l != null) {
                    membersOnline.addAll(l)
                }
//                offset += step
                start += offset
                if (count == (circles - 1)) {//высчитываем остаток элементов на предпоследенй итерации
                    offset = stringUsers.size % 1000
                    end += offset
                } else {
                    end += offset
                }

            } else {
                break
            }
            Thread.sleep(340)
        }
        return membersOnline
    }

    fun getAllGroupMembers(fromUser: Long, groupNameOrId: String): List<Int> {
        val count = 1000
        var offset = 0
        val allGroupMembers: MutableList<Int> = mutableListOf()

        while (true) {
            val members = vkUtils.getGroupMembers(fromUser, groupNameOrId, count, offset)
            if (members != null && members.items.isNotEmpty()) {
                allGroupMembers.addAll(members.items)
                offset += 1000
            } else {
                break
            }
            Thread.sleep(340)
        }
        LOGGER.info(allGroupMembers)

        return allGroupMembers
    }

    fun getMessages(fromUser: Long): GetResponse? {
        val vkUser = vkUserRepository.getOne(fromUser)
        val userActor = UserActor(vkUser.userId.toInt(), vkUser.accessToken)
        val mes = vkApiClient.messages().get(userActor).execute()
        return mes
    }

    fun markAsReadDialog(fromUser: Long, dialog: Int) {
        val vkUser = vkUserRepository.getOne(fromUser)
        val userActor = UserActor(vkUser.userId.toInt(), vkUser.accessToken)

//        vkApiClient.messages().markAsAnsweredDialog(userActor)
    }

    fun findUsers(userId: Int) {

        var users: MutableList<JsonObject> = mutableListOf()
        vkUtils.setUserActor(453439932)
        var startUserId = userId
        while (startUserId < 526097356) {
//            GlobalScope.launch {
//                delay(1000)
            try {
                var temp = vkUtils.getUserInfo25Queries(startUserId)
                LOGGER.info("=============$startUserId================")
                var jsonTemp = temp!!.asJsonArray
                for (it in jsonTemp) {
                    try {
                        if (it.asJsonArray.get(0).asJsonObject.get("counters").asJsonObject.get("friends").toString().toInt() > 1500) {
                            val friendshipUsers = FriendshipUsers()
                            friendshipUsers.userId = it.asJsonArray.get(0).asJsonObject.get("id").toString().toLong()
                            friendshipUsers.friendshipCount = it.asJsonArray.get(0).asJsonObject.get("counters").asJsonObject.get("friends").toString().toInt()
                            friendshipUsers.sex = it.asJsonArray.get(0).asJsonObject.get("sex").toString().toInt()
                            friendshipUsersRepository.save(friendshipUsers)

                            users.add(it.asJsonArray.get(0).asJsonObject.get("counters").asJsonObject)
                            println(it.asJsonArray.get(0).asJsonObject.get("counters").asJsonObject.get("friends"))
                        }
                    } catch (e: Exception) {
//                        e.printStackTrace()
//                            LOGGER.info("такой пользователь нам не нужен.")
                    }
                }
//                    println(temp)
            } catch (e: Exception) {
                    e.printStackTrace()
            } finally {
                startUserId += 25
//                Thread.sleep(340)
            }
//            }
        }
        println()
    }

    fun sendRequestForFriendship(toUser: Long, captchaSid: String = "", captchaKey: String = "") {
        vkUtils.setUserActor(419112073)

        val response = vkUtils.areFriend(toUser)
        val userStatus = response!![0].friendStatus.value
        LOGGER.info(response)

        when (userStatus) {
            0 -> {
                vkUtils.addFriend(toUser, captchaSid, captchaKey)
                LOGGER.info("Заявка пользователю https://vk.com/id$toUser отправлена.")
            }
            1 -> LOGGER.info("Уже отправлена заявка/подписка пользователю $toUser, повторно отправлять не нужно.")
            2 -> LOGGER.info("Имеется входящая заявка/подписка от пользователя, нужно его принять в друзья.")
            3 -> LOGGER.info("Пользователь является другом, ничего делать не нужно.")
        }
    }
}

//Лимит сообщений Вконтакте
//При отправке сообщений придерживайтесь следующих лимитов.

//Лимит на отправку сообщений не друзьям	20 / в сутки
//Лимит на отправку сообщений в ЛС друзьям	Неограниченно
//Посты на стены пользователей	40 / в сутки
//Посты на стены групп, пабликов и встреч	100 / в сутки
//Рассылка по комментариям	100 / в сутки

//Лимиты для групп Вконтакте
//Лимит приглашений друзей в группу	40 / в сутки
//Макс. количество групп в которые можно вступить	5000
//Макс. количество ссылок в группе	100
//Макс. количество видео в группе	10000
//Приглашение из группы во встречу	5000 / сутки
//Количество постов в одной группе	50 / сутки

//Прочие лимиты и ограничения
//Лимит лайков Вконтакте	500 / в сутки
//Лимит голосов Вконтакте	100 / в сутки
package ru.program.vkUsersSkill.servicies

import org.springframework.beans.factory.annotation.Autowired
import ru.program.vkUsersSkill.models.GoodWord
import ru.program.vkUsersSkill.repositories.GoodWordRepository
import ru.program.vkUsersSkill.repositories.PhraseTypeRepository

@Autowired
lateinit var goodWordRepository: GoodWordRepository
@Autowired
lateinit var phraseTypeRepository: PhraseTypeRepository

class PhrasesService {
    fun addPhraseToDB (phrase: String, phraseType: Int) {
        val ph: GoodWord? = goodWordRepository.findGoodWordByPhrase(phrase)
        if (ph == null || !phrase.equals(ph.phrase, true)) {
            goodWordRepository.save(GoodWord(phrase, phraseTypeRepository.getOne(phraseType)))
        } else {
            println("Такая поговорка есть")
        }
    }
}
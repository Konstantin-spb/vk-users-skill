package ru.program.vkUsersSkill.init

import com.vk.api.sdk.exceptions.ApiCaptchaException
import com.vk.api.sdk.queries.likes.LikesType
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.program.vkUsersSkill.models.VkGroup
import ru.program.vkUsersSkill.models.VkUser
import ru.program.vkUsersSkill.repositories.FriendshipUsersRepository
import ru.program.vkUsersSkill.repositories.VkGroupRepository
import ru.program.vkUsersSkill.repositories.VkUserRepository
import ru.program.vkUsersSkill.servicies.VkService
import java.util.*
import kotlin.random.Random

@Component
class StartHere {
    val LOGGER = LogManager.getLogger(StartHere::class.java)
    @Autowired
    lateinit var vkUserRepository: VkUserRepository

    @Autowired
    lateinit var vkGroupRepository: VkGroupRepository

    @Autowired
    lateinit var vkService: VkService

    @Autowired
    lateinit var friendshipUsersRepository: FriendshipUsersRepository

    fun runUserSkillUpperHere() {
        val fromUser = vkUserRepository.getVkUserByAlias("Калашникова Анна")
        val toUser = vkUserRepository.getVkUserByAlias("Остудин Владимир")

        val groupNameOrId = "pro_mens"
//        val groupNameOrId = "erotic_girls_forever"
//        val groupNameOrId = "porno_sex_znakomstva_vk"
//        val groupNameOrId = "kinonov_in"

        /*Begin - Получить сообщения пользователя*/
//        val mes = vkService.getMessages(fromUser.userId)
        /*End - Получить сообщения пользователя*/
//        for (dialog in mes.items) {
//            val unreadDialog = dialog.unread
//        }
//        println(mes)

/*Лайкнуть случайные аватарки анкет из определенной группы - Begin*/
//        val membersOnline = vkService.getAllOnlineMembersFromGroup(fromUser.userId, groupNameOrId)
//        Thread.sleep(1000)
//        var count = 0
//        while (count in 0..100) {
//            membersOnline.shuffle()
//            vkService.addLikeToAva(fromUser.userId, membersOnline.first().id.toLong())
//            count++
//            LOGGER.info("================ Поставленно ${count} лайков =====================")
//            Thread.sleep(10000)
//            vkService.sendMessage(fromUser.userId, toUser.userId, "", Random.nextInt(1, 100) > 50)//Если больше 50, то стикер, если меньше, то сообщение
//            Thread.sleep(15000)
//        }
/*Лайкнуть случайные аватарки анкет из определенной группы - End*/


        var toStop = false
        var groupIdText: String
        var groupLink: String = ""
        var vkGroups: MutableList<VkGroup>

        while (!toStop) {
            try {
                sendRequestsToFriendship()
                Thread.sleep(13000)

                setNewLikeToRandomUser(fromUser, toUser, groupNameOrId)

                Thread.sleep(33000)


                vkGroups = vkGroupRepository.findAll()
                vkGroups.shuffle()
                groupLink = vkGroups.first().vkGroupLink
                groupIdText = when {
                    groupLink.startsWith("""http://vk.com/public""", true) -> groupLink.replace("""http://vk.com/public""", "")
                    groupLink.startsWith("""http://vk.com/club""", true) -> groupLink.replace("""http://vk.com/club""", "")
                    else -> groupLink.replace("""http://vk.com/""", "")
                }
                //Если комент не был добавлен, то попробовать добавить в другую группу комент
                if (vkService.commentsCount < 36 && !vkService.addCommentToPost(groupIdText, fromUser.userId).contains("commentId")) {
                    continue
                } else if (vkService.commentsCount >= 36) {
                    break
                }
                vkService.commentsCount++
                LOGGER.info(groupLink)
                Thread.sleep(10000)

                vkService.sendMessage(fromUser.userId, toUser.userId, "", Random.nextInt(1, 100) > 50)//Если больше 50, то стикер, если меньше, то сообщение

                //Разослать запросы в друзья - begin
                for (index in 1..1) {
                    sendRequestsToFriendship()
                    Thread.sleep(62000)
                }
                //Разослать запросы в друзья - end

                setNewLikeToRandomUser(fromUser, toUser, groupNameOrId)
                Thread.sleep(23000)

                vkService.sendMessage(fromUser.userId, toUser.userId, "", Random.nextInt(1, 100) > 50)//Если больше 50, то стикер, если меньше, то сообщение
                Thread.sleep(2000000)
            } catch (e: Exception) {
                e.printStackTrace()
                LOGGER.error(groupLink)
            }
        }
    }

    fun sendRequestsToFriendship() {
        var possibleFriends = friendshipUsersRepository.findAll()
        possibleFriends = possibleFriends.shuffled()
        try {
            vkService.sendRequestForFriendship(possibleFriends[0].userId)
        } catch (e: ApiCaptchaException) {
            val captchaSid = e.sid
            val captchaImg = e.image

            LOGGER.info(captchaImg)
            val input = Scanner(System.`in`)
            val captchaKey = input.next()

            e.printStackTrace()
            vkService.sendRequestForFriendship(possibleFriends[0].userId, captchaSid, captchaKey)
        }
    }

    fun setNewLikeToRandomUser(fromUser: VkUser, toUser: VkUser,  groupNameOrId: String) {
        /*Лайкнуть случайные аватарки анкет из определенной группы - Begin*/
        val membersOnline = vkService.getAllOnlineMembersFromGroup(fromUser.userId, groupNameOrId)
        Thread.sleep(1000)
        var count = 0
        while (count in 0..10) {
            membersOnline.shuffle()
            vkService.addLikeToAva(fromUser.userId, membersOnline.first().id.toLong())
            count++
            LOGGER.info("================ Поставленно ${count} лайков =====================")
            Thread.sleep(6000)
            vkService.sendMessage(fromUser.userId, toUser.userId, "", Random.nextInt(1, 100) > 50)//Если больше 50, то стикер, если меньше, то сообщение
            Thread.sleep(9000)
        }
/*Лайкнуть случайные аватарки анкет из определенной группы - End*/
    }
}
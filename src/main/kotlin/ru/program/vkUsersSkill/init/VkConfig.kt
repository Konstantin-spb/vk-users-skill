package ru.program.vkUsersSkill.init

import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.httpclient.HttpTransportClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import ru.program.vkUsersSkill.utils.HttpTransportClientCustom
import ru.program.vkUsersSkill.utils.ProxyForVk

//@Configuration
//@Component
//@Scope("prototype")
class VkConfig {
//    @Bean
    fun getVkApiClient (proxy: String): VkApiClient {
//        val transportClient = HttpTransportClient.getInstance()
        var transportClient = HttpTransportClientCustom(proxy = proxy)
//        transportClient = HttpTransportClientCustom.getInstance()
        return VkApiClient(transportClient)
    }
}
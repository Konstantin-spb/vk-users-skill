package ru.program.vkUsersSkill.models

import javax.persistence.*

@Entity
@Table(name = "vk_groups")
data class VkGroup(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", unique = true, nullable = false)
        var id: Long = -1,
        @Column(name = "group_link", unique = true, nullable = false)
        var vkGroupLink: String = ""
)
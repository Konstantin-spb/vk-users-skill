package ru.program.vkUsersSkill.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "friendship_users")
data class FriendshipUsers(
        @Id
        @Column(name = "user_id")
        var userId: Long = 0L,

        @Column(name = "friends_count")
        var friendshipCount: Int = 0,

        @Column(name = "sex")
        var sex: Int = 0
)
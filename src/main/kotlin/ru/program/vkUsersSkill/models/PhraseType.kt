package ru.program.vkUsersSkill.models

import javax.persistence.*

@Entity
@Table(name = "phrases_types")
data class PhraseType(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val Id: Int = -1,
        @Column(name = "phrase_type")
        var phrase_type: String,
        @OneToMany(fetch = FetchType.LAZY, mappedBy = "phraseType")
        var goodWord: MutableList<GoodWord> = mutableListOf()
)
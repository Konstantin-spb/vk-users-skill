package ru.program.vkUsersSkill.models

import javax.persistence.*

@Entity
@Table(name = "good_words")
data class GoodWord(
        @Column(name = "phrase", unique = true, nullable = false)
        var phrase: String = "",
        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "phrase_type_id")
        var phraseType: PhraseType? = null,@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id:Long = -1
) {
        override fun toString(): String {
                return phrase
        }
}
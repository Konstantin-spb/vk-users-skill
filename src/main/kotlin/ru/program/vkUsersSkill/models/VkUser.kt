package ru.program.vkUsersSkill.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table (name = "vk_users")
data class VkUser(
    @Id
    @Column(name = "user_id")
    var userId: Long = 0L,

    @Column(name = "access_token")
    var accessToken: String = "",

    @Column(name = "alias")
    var alias: String = "",

    @Column(name = "login")
    var login: String = "",

    @Column(name = "password")
    var password: String = ""
)
package ru.program.vkUsersSkill

import com.vk.api.sdk.client.VkApiClient
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import ru.program.vkUsersSkill.init.StartHere
import ru.program.vkUsersSkill.init.VkConfig
import ru.program.vkUsersSkill.servicies.VkService
import ru.program.vkUsersSkill.utils.HttpTransportClientCustom
import ru.program.vkUsersSkill.utils.ProxyForVk

@SpringBootApplication
class VkUsersSkillApplication

fun main(args: Array<String>) {
    val context = runApplication<VkUsersSkillApplication>(*args)
//Поиск пользователей с друзьями более 1500
    val vkService = context.getBean(VkService::class.java)
    val vkApiClient: VkApiClient = VkConfig().getVkApiClient("http://94.180.249.187:38051")
//        vkApiClient = VkConfig().getVkApiClient(args[1])
    vkService.vkApiClient = vkApiClient
    vkService.vkUtils.vkApiClient = vkApiClient
//    vkService.findUsers(args[0].toInt())
    vkService.findUsers(2684305)

    //добавление пользователя в друзья
//	val start = context.getBean(StartHere::class.java)
//	start.sendRequestsToFriendship()

//	val vkApiClient = VkConfig().getVkApiClient(args[0])
//	val start = context.getBean(StartHere::class.java)
//	start.vkService.vkApiClient = vkApiClient
//	start.vkService.vkUtils.vkApiClient = vkApiClient
//	start.runUserSkillUpperHere()
}
//Для группы
//is_closed,deactivated,type,is_member,age_limits,can_create_topic,can_post,wall
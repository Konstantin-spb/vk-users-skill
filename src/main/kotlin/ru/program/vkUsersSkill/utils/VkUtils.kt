package ru.program.vkUsersSkill.utils

import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.objects.groups.responses.GetMembersResponse
import com.vk.api.sdk.objects.users.UserXtrCounters
import com.vk.api.sdk.queries.likes.LikesType
import com.vk.api.sdk.queries.users.UserField
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import ru.program.vkUsersSkill.repositories.VkUserRepository
import com.google.gson.JsonElement
import com.vk.api.sdk.objects.friends.FriendStatus
import com.vk.api.sdk.objects.friends.responses.AddResponse
import ru.program.vkUsersSkill.init.VkConfig


@Component
@Scope("prototype")
class VkUtils {
    @Autowired
    lateinit var vkUserRepository: VkUserRepository

//    @Autowired
    lateinit var vkApiClient: VkApiClient

    lateinit var userActor: UserActor

    fun setProxy(proxy: String) {

    }

    fun setUserActor(fromUser: Long) {
        this.userActor = getUserActor(fromUser)
    }

    fun getUserActor(userId: Long): UserActor {
        val vkUser = vkUserRepository.getOne(userId)
        return UserActor(vkUser.userId.toInt(), vkUser.accessToken)
    }

    fun getGroupMembers(fromUser: Long, groupNameOrId: String, count: Int, offset: Int): GetMembersResponse? {
        return vkApiClient.groups().getMembers(getUserActor(fromUser)).groupId(groupNameOrId).count(count).offset(offset).execute()
    }

    fun checkUserOnline(fromUser: Long, usersForCheck: List<String>): MutableList<UserXtrCounters>? {
        //не больше 1000 пользователей в запросе
        val userActor = getUserActor(fromUser)
        val userFieldList = listOf<UserField>(UserField.ONLINE, UserField.SEX, UserField.FOLLOWERS_COUNT)
        var users = vkApiClient.users().get(userActor).userIds(usersForCheck).fields(userFieldList).execute()
        users = users.filter { it.isOnline }
        return users
    }

    fun isLiked(fromUser: Long, toUser: Long, itemId: Long): Boolean {
        val userActor = getUserActor(fromUser)
        return vkApiClient.likes().isLiked(userActor, LikesType.PHOTO, itemId.toInt()).ownerId(toUser.toInt()).execute().isLiked
    }

    fun getUserInfo25Queries(userId: Int): JsonElement? {

        val fields: MutableList<UserField> = mutableListOf()
        fields.add(UserField.SEX)
        fields.add(UserField.COUNTERS)
        fields.add(UserField.LAST_SEEN)
        val response = vkApiClient.execute().batch(userActor,
                vkApiClient.users().get(userActor).fields(fields).userIds(userId.toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 1).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 2).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 3).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 4).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 5).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 6).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 7).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 8).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 9).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 10).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 11).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 12).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 13).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 14).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 15).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 16).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 17).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 18).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 19).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 20).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 21).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 22).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 23).toString()),
                vkApiClient.users().get(userActor).fields(fields).userIds((userId + 24).toString())
//                vkApiClient.users().get(userActor).fields(fields).userIds(userId + 25)
        ).execute()

        return response
    }

    fun areFriend(toUser: Long): MutableList<FriendStatus>? {
        return vkApiClient.friends().areFriends(userActor, toUser.toInt()).needSign(false).execute()
    }

    fun addFriend(toUser: Long, captchaSid: String = "", captchaKey: String = ""): AddResponse? {
        var request = vkApiClient.friends().add(userActor, toUser.toInt())
        if (captchaSid.isNotEmpty() && captchaKey.isNotEmpty()) {
            request.captchaSid(captchaSid)
                    .captchaKey(captchaKey)
        }
        return request.execute()
    }

}
package ru.program.vkUsersSkill.utils

import com.vk.api.sdk.client.ClientResponse
import com.vk.api.sdk.client.TransportClient
import com.vk.api.sdk.httpclient.HttpDeleteWithBody
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.http.Header
import org.apache.http.HttpHost
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.entity.StringEntity
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.impl.client.BasicCookieStore
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.net.SocketException
import java.nio.charset.StandardCharsets
import java.util.HashMap

class HttpTransportClientCustom @JvmOverloads constructor(private val retryAttemptsNetworkErrorCount: Int = DEFAULT_RETRY_ATTEMPTS_NETWORK_ERROR_COUNT, private val retryAttemptsInvalidStatusCount: Int = DEFAULT_RETRY_INVALID_STATUS_COUNT, proxy: String) : TransportClient {

//    var proxy: String = ""

    init {
        val cookieStore = BasicCookieStore()
        val requestConfig = RequestConfig.custom()
                .setSocketTimeout(SOCKET_TIMEOUT_MS)
                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                .setCookieSpec(CookieSpecs.STANDARD)
                .build()

        val connectionManager = PoolingHttpClientConnectionManager()

        connectionManager.maxTotal = MAX_SIMULTANEOUS_CONNECTIONS
        connectionManager.defaultMaxPerRoute = MAX_SIMULTANEOUS_CONNECTIONS

        var httpClientBuilder = HttpClients.custom()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(cookieStore)
                .setUserAgent(USER_AGENT)
//                .setProxy(HttpHost("185.34.23.43", 63238, "http"))
                if (proxy.isNotEmpty()) {
                    httpClientBuilder.setProxy(HttpHost.create(proxy))
                }
        httpClient = httpClientBuilder.build()

    }

    @Throws(IOException::class)
    private fun callWithStatusCheck(request: HttpRequestBase): ClientResponse {
        var response: ClientResponse
        var attempts = 0

        do {
            response = call(request)
            attempts++
        } while (attempts < retryAttemptsInvalidStatusCount && isInvalidGatewayStatus(response.statusCode))

        return response
    }

    private fun isInvalidGatewayStatus(status: Int): Boolean {
        return status == HttpStatus.SC_BAD_GATEWAY || status == HttpStatus.SC_GATEWAY_TIMEOUT
    }

    @Throws(IOException::class)
    private fun call(request: HttpRequestBase): ClientResponse {
        var exception: SocketException? = null
        for (i in 0 until retryAttemptsNetworkErrorCount) {
            try {
                SUPERVISOR.addRequest(request)

                val startTime = System.currentTimeMillis()

                val response = httpClient.execute(request)

                val endTime = System.currentTimeMillis()

                val resultTime = endTime - startTime

                try {
                    response.entity.content.use { content ->
                        val result = IOUtils.toString(content, ENCODING)
                        val responseHeaders = getHeaders(response.allHeaders)
                        val requestHeaders = getHeaders(request.allHeaders)
                        logRequest(request, requestHeaders, response, responseHeaders, result, resultTime)
                        return ClientResponse(response.statusLine.statusCode, result, responseHeaders)
                    }
                } finally {
                    SUPERVISOR.removeRequest(request)
                }
            } catch (e: SocketException) {
                logRequest(request)
                LOG.warn("Network troubles", e)
                exception = e
            }

        }

        throw exception!!
    }

    @Throws(IOException::class)
    private fun getRequestPayload(request: HttpRequestBase): String {
        if (request !is HttpPost) {
            return EMPTY_PAYLOAD
        }

        if (request.entity == null) {
            return EMPTY_PAYLOAD
        }

        if (StringUtils.isNotEmpty(request.entity.contentType.value)) {
            val contentType = request.entity.contentType.value
            if (contentType.contains("multipart/form-data")) {
                return EMPTY_PAYLOAD
            }
        }

        return IOUtils.toString(request.entity.content, StandardCharsets.UTF_8)
    }

    @Throws(IOException::class)
    private fun logRequest(request: HttpRequestBase, requestHeaders: Map<String, String>? = null, response: HttpResponse? = null, responseHeaders: Map<String, String>? = null, body: String? = null, time: Long? = null) {
        if (LOG.isDebugEnabled) {
            val payload = getRequestPayload(request)

            val builder = StringBuilder("\n")
                    .append("Request:\n")
                    .append("\t").append("Headers: ").append(requestHeaders ?: "-").append("\n")
                    .append("\t").append("Method: ").append(request.method).append("\n")
                    .append("\t").append("URI: ").append(request.uri).append("\n")
                    .append("\t").append("Payload: ").append(payload).append("\n")
                    .append("\t").append("Time: ").append(time ?: "-").append("\n")

            if (response != null) {
                builder.append("Response:\n")
                        .append("\t").append("Status: ").append(response.statusLine.toString()).append("\n")
                        .append("\t").append("Headers: ").append(responseHeaders ?: "-").append("\n")
                        .append("\t").append("Body: ").append(body ?: "-").append("\n")
            }

            LOG.debug(builder.toString())
        } else if (LOG.isInfoEnabled) {
            val builder = StringBuilder().append("Request: ").append(request.uri.toURL().toString())
            if (time != null) {
                builder.append("\t\t").append(time)
            }

            LOG.info(builder.toString())
        }
    }

    @Throws(IOException::class)
    override fun get(url: String): ClientResponse {
        return get(url, FORM_CONTENT_TYPE)
    }

    @Throws(IOException::class)
    override fun get(url: String, contentType: String): ClientResponse {
        val request = HttpGet(url)
        request.setHeader(CONTENT_TYPE_HEADER, contentType)
        return callWithStatusCheck(request)
    }

    @Throws(IOException::class)
    override fun post(url: String): ClientResponse {
        return post(url, null)
    }

    @Throws(IOException::class)
    override fun post(url: String, body: String?): ClientResponse {
        return post(url, body, FORM_CONTENT_TYPE)
    }

    @Throws(IOException::class)
    override fun post(url: String, body: String?, contentType: String): ClientResponse {
        val request = HttpPost(url)
        request.setHeader(CONTENT_TYPE_HEADER, contentType)
        if (body != null) {
            request.entity = StringEntity(body, "UTF-8")
        }

        return callWithStatusCheck(request)
    }

    @Throws(IOException::class)
    override fun post(url: String, fileName: String, file: File): ClientResponse {
        val request = HttpPost(url)
        val fileBody = FileBody(file)
        val entity = MultipartEntityBuilder
                .create()
                .addPart(fileName, fileBody).build()

        request.entity = entity
        return callWithStatusCheck(request)
    }

    @Throws(IOException::class)
    override fun delete(url: String): ClientResponse {
        return delete(url, null, FORM_CONTENT_TYPE)
    }

    @Throws(IOException::class)
    override fun delete(url: String, body: String): ClientResponse {
        return delete(url, body, FORM_CONTENT_TYPE)
    }

    @Throws(IOException::class)
    override fun delete(url: String, body: String?, contentType: String): ClientResponse {
        val request = HttpDeleteWithBody(url)
        request.setHeader(CONTENT_TYPE_HEADER, contentType)
        if (body != null) {
            request.entity = StringEntity(body)
        }

        return callWithStatusCheck(request)
    }

    companion object {

        private val LOG = LoggerFactory.getLogger(HttpTransportClientCustom::class.java)

        private val ENCODING = "UTF-8"
        private val FORM_CONTENT_TYPE = "application/x-www-form-urlencoded"
        private val CONTENT_TYPE_HEADER = "Content-Type"
//        private val USER_AGENT = "Java VK SDK/0.5.12"
        private val USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"

        private val EMPTY_PAYLOAD = "-"

        private val MAX_SIMULTANEOUS_CONNECTIONS = 300
        private val DEFAULT_RETRY_ATTEMPTS_NETWORK_ERROR_COUNT = 3
        private val DEFAULT_RETRY_INVALID_STATUS_COUNT = 3
        private val FULL_CONNECTION_TIMEOUT_S = 600
        private val CONNECTION_TIMEOUT_MS = 500
        private val SOCKET_TIMEOUT_MS = FULL_CONNECTION_TIMEOUT_S * 1000

        private val SUPERVISOR = ConnectionsSupervisor()
        private var instance: HttpTransportClientCustom? = null
        private lateinit var httpClient: HttpClient

//        fun getInstance(): HttpTransportClientCustom {
//            if (instance == null) {
//                instance = HttpTransportClientCustom()
//            }
//            return instance as HttpTransportClientCustom
//        }

        private fun getHeaders(headers: Array<Header>): Map<String, String> {
            val result = HashMap<String, String>()
            for (header in headers) {
                result[header.name] = header.value
            }

            return result
        }
    }
}

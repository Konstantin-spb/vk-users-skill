package ru.program.vkUsersSkill.utils

import org.apache.http.client.methods.HttpUriRequest
import org.slf4j.LoggerFactory
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

class ConnectionsSupervisor internal constructor() : Thread() {

    private val streams = ConcurrentHashMap<HttpUriRequest, Long>()

    init {
        isDaemon = true
        name = "Connections supervisor"
    }

    override fun run() {
        while (true) {
            try {
                Thread.sleep(CONNECTIONS_SUPERVISOR_WAIT_MS.toLong())
            } catch (ignored: InterruptedException) {
                //nop
            }

            val time = Instant.now().epochSecond
            streams.entries.stream().filter { entry -> time > entry.value }.forEach { entry ->
                val request = entry.key
                // double check
                if (streams.containsKey(request)) {
                    LOG.error(String.format("HttpUriRequest killed after timeout (%d sec.) exceeded: %s",
                            FULL_CONNECTION_TIMEOUT_S,
                            request))

                    request.abort()
                    removeRequest(request)
                }
            }
        }
    }

    internal fun addRequest(request: HttpUriRequest) {
        streams[request] = Instant.now().epochSecond + FULL_CONNECTION_TIMEOUT_S.toLong() + WAIT_BEFORE_KILL_REQUEST_S.toLong()
    }

    internal fun removeRequest(request: HttpUriRequest) {
        streams.remove(request)
    }

    companion object {

        private val LOG = LoggerFactory.getLogger(ConnectionsSupervisor::class.java)

        private val FULL_CONNECTION_TIMEOUT_S = 60
        private val WAIT_BEFORE_KILL_REQUEST_S = 10
        private val CONNECTIONS_SUPERVISOR_WAIT_MS = 1000
    }
}

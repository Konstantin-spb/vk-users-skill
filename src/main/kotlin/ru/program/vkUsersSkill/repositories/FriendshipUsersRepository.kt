package ru.program.vkUsersSkill.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.program.vkUsersSkill.models.FriendshipUsers

interface FriendshipUsersRepository: JpaRepository<FriendshipUsers, Long>
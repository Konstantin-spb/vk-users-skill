package ru.program.vkUsersSkill.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.program.vkUsersSkill.models.GoodWord

interface GoodWordRepository: JpaRepository<GoodWord, Long> {
    fun getGoodWordByPhrase(phrase: String): GoodWord
    fun findGoodWordByPhrase(phrase: String): GoodWord?
}
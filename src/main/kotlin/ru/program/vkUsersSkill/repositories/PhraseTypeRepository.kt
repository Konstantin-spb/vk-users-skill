package ru.program.vkUsersSkill.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.program.vkUsersSkill.models.PhraseType

interface PhraseTypeRepository: JpaRepository<PhraseType, Int> {
}
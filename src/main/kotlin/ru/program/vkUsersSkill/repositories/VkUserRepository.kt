package ru.program.vkUsersSkill.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.program.vkUsersSkill.models.VkUser

interface VkUserRepository: JpaRepository<VkUser, Long> {
    fun getVkUserByAlias(alias: String): VkUser
}
package ru.program.vkUsersSkill.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.program.vkUsersSkill.models.VkGroup

interface VkGroupRepository: JpaRepository<VkGroup, Long>